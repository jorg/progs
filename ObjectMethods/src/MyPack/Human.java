package MyPack;

import java.io.Serializable;

public class Human implements Cloneable, Serializable{
	static final long serialVersionUID = 1L;
	private int age =0;
	private String name = "";
	
	public Human (String name, int age) {
		this.name = name;
		this.age = age;
	}
		
	
public Human clone() throws CloneNotSupportedException {
		
		return (Human) super.clone();
	}
		
	public int hashCode() {
		
		return name.hashCode() + age;
	}
	
	public String toString() {
		
		return name + " " + Integer.toString(age);
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		else {
			Human current = (Human)obj;
			return  this.name == current.name && this.age == current.age; 
		}
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}
	
}
