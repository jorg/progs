package MyPack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		Human arr[] = new Human[10];
		Office offWrite = new Office();
		Office offRead = new Office();
		 
		boolean fin = false;
		Scanner sc = new Scanner(System.in);
		String UserResponse = "";
		while(!fin)	{
			
			System.out.println("Press'1' to generate and save Humans");
			System.out.println("Press'2' to load list of Humans");
				if (sc.hasNext()) {
					
					UserResponse = sc.next();
					
					if (UserResponse.equalsIgnoreCase("1")) {
						fin = true;
						sc.close();
						
						for (int i = 0; i < arr.length; i++) {
							arr[i] = PersonGenerator.genHuman();
							
						}
						offWrite.setContainer(arr);
						WriteClass(offWrite);
						System.out.println(Arrays.toString(arr));
						
					}
						if (UserResponse.equalsIgnoreCase("2")) {
						fin = true;
						sc.close();
						
						File classFile = new File("D:\\temp.out");
						if (classFile.exists()) {
							offRead = ReadClass("D:\\temp.out");
							System.out.println(Arrays.toString(offRead.getContainer()));
						}
						else {
							System.out.println("File not found");
						}
						
					}
								
			}
		}
	}
	
	public static void WriteClass(Office classToWrite) {
		ObjectOutputStream oos = null;
		FileOutputStream fos = null;
		try { 
			fos = new FileOutputStream("D:\\temp.out");
	        oos = new ObjectOutputStream(fos);
	        oos.writeObject(classToWrite);
		}
		
		catch (IOException e ) {
			System.out.println("Ooops!!");
			
		}
		finally {
			try {
				oos.close();
			} 
			catch (IOException e) {
			}
		}
		
	}
	
	public static Office ReadClass(String path) {
		
		FileInputStream fis =null; 
        ObjectInputStream oin = null;
        Office offRead = new Office();
        
        try {
	        	fis= new FileInputStream(path);
	        	oin = new ObjectInputStream(fis);
	            offRead = (Office) oin.readObject();
  
        } 
        catch (IOException | ClassNotFoundException ex) 
        { 
        	System.out.println("Ooops!!");
        }
        finally {
        	try {
				oin.close();
			} catch (IOException e) {
				
			}
        }
        return offRead;
		
	}
	
}

