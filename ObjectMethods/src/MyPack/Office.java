package MyPack;

import java.io.Serializable;

public class Office implements Serializable {
	private static final long serialVersionUID = 3638043625421423847L;
	private Human container[] = null;
	
	public Human[] getContainer() {
		return container;
	}
	public void setContainer(Human container[]) {
		this.container = container.clone();
	}
	
}
