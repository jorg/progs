/*��������� �������� ��������� �������:
	1. �������� �������� �������
	2. ����������� ������� ������ �� ������ ����� ��� �������� �������� ��� ��������� ��������
	3. ����������� ������ ���� ����� � ������ ��������� ��������
	4. ���� ������� ������������ ���������� �����:
	4�. ���������������� ������� ��������� ��������
	4�. ����������� ���������� ������� ������ � ���������� � ���� ����� ���������� ��������
	5. ���� ������ ���� - ������ �������������� �������
	6. ���� ������� �� ������������� �����������, ��� ����� ���������� ������� ������ � ������ ������� �������� 
		�.�. ���� �� ������ ����� ��������� ���������� ����� ��� �����
	� ������ ���� ����������� ����� �� ���� � �������, ��������� NullPointerException. �� ��������������� � 
	���� ������� ������������� �����������, ������� ��������� �������� ����������������.*/     

import java.io.File;
import java.util.Arrays;

public class Main1 {

	public static void main(String[] args) {
				 
		int y = 0;
		//�������� ������� �� �������� ���������� �����
		File f = new File("C:\\");
		long t1 = System.currentTimeMillis();
		// ������� ������������ ������ � ������ ��������� 
		y = func(f);
	//	System.out.println( "Folders and files found: " + y);
		System.out.println("Time spent: " + (System.currentTimeMillis() - t1) + " ms");
		
}
	public static int func(File f) {

		File[]  fileArr= null;
		//��������� ������� ������ � ��������� �� ������ ����������� � �������� ��������� 
		fileArr = f.listFiles();
		int res = 0;
			//������ �� ������ � ��������� �� ����������� ������� ���� 
			for(File file : fileArr) {
				// ���������� �������� � ��������� 
				if (file.isDirectory() && file.exists() && file != null) 
				{
					
					if (file.getName().charAt(0) == 'A' || file.getName().charAt(0) == 'a') {
						try {
							//���� ������� ������������� �����������, �������������� ������� � 
							//���������� �������� ������� ������
							res = res + func(file) + 1;
							System.out.println("Folder found: " + file.getAbsolutePath());
						}
						catch (NullPointerException e ) {
							// ������ ����������� � ������ ���������� ������� � ��������.
							// ���� ���� ������� �� �������� ��� �����    
							res = res + 1;
							//System.out.println(file) test;
						}
					}
					else {
						try {
							//���� ������� �� ������������� ����������, ������� ������ ���������� ����������.
							//�.�. � ����� ��������� ��� ����� ����� ��������� ������ ��� �����.
							// ������� �� ����������������
							res = res + func(file) + 0;
							
						}
						catch (NullPointerException e ) {
							//���������� ����� ���������� ���� ������� �� �������� ��� �����
							res = res + 0;
							//System.out.println(file);
						}
					}
					
				}
				//��������� ������.
				else if (file.isFile() && file.exists() && file != null) {
					// ���� ���� ��� �������� ������ ������������� ������� 
					if (file.getName().charAt(0) =='A' || file.getName().charAt(0) =='a') {
							res = res + 1;
							System.out.println("File found: " + file.getAbsolutePath());
					}
				}
			}
			
			return res;
		}
	}